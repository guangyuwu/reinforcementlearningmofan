from src.rl4_sarsa_lambda_maze.maze_env import Maze
from src.rl4_sarsa_lambda_maze.rl_brain import SarsaLambdaTable


def update():
    for episode in range(100):
        print('-' * 20 + f' {episode} ' + '-' * 20)
        # initial state (observation)
        state = env.reset()

        # RL choose action based on current state
        action = RL.choose_action(state)

        while True:
            print(state)
            # fresh env
            env.render()

            # RL takes action and get next state and reward
            next_state, reward, done = env.step(action)

            # RL chooses next action based on next state
            next_action = RL.choose_action(next_state)

            # RL learns from this transition
            RL.learn(state, action, reward, next_state, next_action)

            # swap state
            state = next_state
            action = next_action

            # break while loop when end of this episode
            if done:
                break

    # end of game
    print('game over')
    env.destroy()


if __name__ == '__main__':
    env = Maze()
    RL = SarsaLambdaTable(actions=list(range(env.n_actions)), learning_rate=0.1)
    env.after(100, update)
    env.mainloop()

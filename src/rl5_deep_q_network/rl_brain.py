"""
This part of code is the Q learning brain, which is a brain of the agent.
All decisions are made in here.
"""

from abc import ABCMeta

import numpy as np
import tensorflow as tf

np.random.seed(1)
tf.set_random_seed(1)


# Deep Q Network off-policy
class DeepQNetwork:
    __metaclass__ = ABCMeta

    def __init__(self,
                 n_actions,
                 n_features,
                 learning_rate=0.01,
                 reward_decay=0.9,
                 e_greedy=0.9,
                 replace_target_iter=300,
                 memory_size=500,
                 batch_size=32,
                 e_greedy_increment=0,
                 output_graph=False):
        # 4 actions: ['u', 'd', 'l', 'r']
        self.n_actions = n_actions
        # 2 features: (x, y)
        self.n_features = n_features
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon_max = e_greedy
        self.replace_target_iter = replace_target_iter
        self.memory_size = memory_size
        self.batch_size = batch_size
        self.epsilon_increment = e_greedy_increment
        self.epsilon = 0 if e_greedy_increment != 0 else self.epsilon_max

        # total learning step
        self.learn_step_counter = 0

        # initialize zero memory [state, action, reward, next_state]
        # shape (2000, 6)
        # 6 elements includes [state_x, state_y, action, reward, next_state_x,
        # next_state_y]
        self.memory = np.zeros((self.memory_size, n_features * 2 + 2))

        # memory counter is used to insert/update transitions
        self.memory_counter = 0

        # consist of [target_net, evaluate_net]
        self._build_net()
        t_params = tf.get_collection('target_net_params')
        e_params = tf.get_collection('eval_net_params')
        self.replace_target_op = [tf.assign(t, e)
                                  for t, e in zip(t_params, e_params)]

        self.sess = tf.Session()

        if output_graph:
            # $ tensorboard --logdir=src/rl5_deep_q_network/logs
            # tf.train.SummaryWriter soon be deprecated, use following
            tf.summary.FileWriter("logs/", self.sess.graph)

        self.sess.run(tf.global_variables_initializer())
        self.cost_his = []

    def _build_net(self):
        # number of nodes in first layer
        n_l1 = 10
        w_initializer = tf.random_normal_initializer(0., 0.3)
        b_initializer = tf.constant_initializer(0.1)

        # ------------------ build evaluate_net ------------------
        # input of current state, shape (?, 2)
        self.state = tf.placeholder(tf.float32, [None, self.n_features],
                                    name='state')
        # for calculating loss, shape (?, 4)
        self.q_target = tf.placeholder(tf.float32, [None, self.n_actions],
                                       name='q_target')

        with tf.variable_scope('eval_net'):
            # c_names(collections_names) are the collections to store variables
            c_names = ['eval_net_params', tf.GraphKeys.GLOBAL_VARIABLES]

            # first layer. collections is used later when assign to target net
            with tf.variable_scope('l1'):
                # weights, shape (2, 10)
                w1 = tf.get_variable('w1',
                                     [self.n_features, n_l1],
                                     initializer=w_initializer,
                                     collections=c_names)
                # bias, shape (1, 10)
                b1 = tf.get_variable('b1',
                                     [1, n_l1],
                                     initializer=b_initializer,
                                     collections=c_names)
                # output of first layer, shape (?, 10)
                l1 = tf.nn.relu(tf.matmul(self.state, w1) + b1)

            # second layer. collections is used later when assign to target net
            with tf.variable_scope('l2'):
                # weights, shape (10, 4)
                w2 = tf.get_variable('w2',
                                     [n_l1, self.n_actions],
                                     initializer=w_initializer,
                                     collections=c_names)
                # bias, shape (1, 4)
                b2 = tf.get_variable('b2', [1, self.n_actions],  # shape (1, 4)
                                     initializer=b_initializer,
                                     collections=c_names)
                # output of second layer, shape (?, 4)
                self.q_eval = tf.matmul(l1, w2) + b2

        with tf.variable_scope('loss'):
            self.loss_op = tf.reduce_mean(
                tf.squared_difference(self.q_target, self.q_eval))
        with tf.variable_scope('train'):
            self._train_op = tf.train.RMSPropOptimizer(self.lr).minimize(
                self.loss_op)

        # ------------------ build target_net ------------------
        # input of next state, shape (?, 2)
        self.next_state = tf.placeholder(tf.float32, [None, self.n_features],
                                         name='next_state')
        with tf.variable_scope('target_net'):
            # c_names(collections_names) are the collections to store variables
            c_names = ['target_net_params', tf.GraphKeys.GLOBAL_VARIABLES]

            # first layer. collections is used later when assign to target net
            with tf.variable_scope('l1'):
                # weights, shape (2, 10)
                w1 = tf.get_variable('w1',
                                     [self.n_features, n_l1],
                                     initializer=w_initializer,
                                     collections=c_names)
                # bias, shape (1, 10)
                b1 = tf.get_variable('b1',
                                     [1, n_l1],
                                     initializer=b_initializer,
                                     collections=c_names)
                # output of first layer, shape (?, 10)
                l1 = tf.nn.relu(tf.matmul(self.next_state, w1) + b1)

            # second layer. collections is used later when assign to target net
            with tf.variable_scope('l2'):
                # weights, shape (10, 4)
                w2 = tf.get_variable('w2',
                                     [n_l1, self.n_actions],
                                     initializer=w_initializer,
                                     collections=c_names)
                # bias, shape (1, 4)
                b2 = tf.get_variable('b2', [1, self.n_actions],
                                     initializer=b_initializer,
                                     collections=c_names)
                # output of second layer, shape (?, 4)
                self.q_next = tf.matmul(l1, w2) + b2

    def store_transition(self, state, action, reward, next_state):
        transition = np.hstack((state, [action, reward], next_state))

        # replace the old memory with new memory
        index = self.memory_counter % self.memory_size
        self.memory[index, :] = transition

        self.memory_counter += 1

    def choose_action(self, state):
        if np.random.uniform() < self.epsilon:
            # to have batch dimension when feed into tf placeholder
            state = state[np.newaxis, :]
            # forward feed the state and get q value for every actions
            actions_value = self.sess.run(self.q_eval,
                                          feed_dict={self.state: state})
            action = np.argmax(actions_value)
        else:
            action = np.random.randint(0, self.n_actions)
        return action

    def learn(self):
        # check to replace target parameters
        if self.learn_step_counter % self.replace_target_iter == 0:
            self.sess.run(self.replace_target_op)
            print(f'target_params_replaced '
                  f'{self.learn_step_counter // self.replace_target_iter}')

        # sample batch memory from all memory
        sample_indices = np.random.choice(
            min(self.memory_size, self.memory_counter), size=self.batch_size)

        batch_memory = self.memory[sample_indices, :]

        # q_next, q_eval = self.sess.run(
        #     # self.q_next is output of target_net
        #     # self.q_eval is output of eval_net
        #     [self.q_next, self.q_eval],
        #     feed_dict={
        #         # self.next_state is input of target_net
        #         # self.next_state <- last two elements (x, y) in batch_memory
        #         self.next_state: batch_memory[:, -self.n_features:],
        #         # self.state is input of eval_net
        #         # self.state <- first two elements (x, y) in batch_memory
        #         self.state: batch_memory[:, :self.n_features],
        #     })
        q_eval = self.sess.run(
            # self.q_eval is output of eval_net
            self.q_eval,
            feed_dict={
                # self.state is input of eval_net
                # self.state <- first two elements (x, y) in batch_memory
                self.state: batch_memory[:, :self.n_features],
            })
        q_next = self.sess.run(
            # self.q_next is output of target_net
            self.q_next,
            feed_dict={
                # self.next_state is input of target_net
                # self.next_state <- last two elements (x, y) in batch_memory
                self.next_state: batch_memory[:, -self.n_features:],
            })

        # update rewards w.r.t actions
        batch_index = list(range(self.batch_size))
        eval_act_index = batch_memory[:, self.n_features].astype(int)
        reward = batch_memory[:, self.n_features + 1]
        q_eval[batch_index, eval_act_index] = reward + self.gamma * np.max(
            q_next, axis=1)

        # train eval network
        # self.state -> eval_net -> self.q_eval
        # self.q_target, self.q_eval -> self.loss_op
        # self.loss_op -> self._train_op -> cost
        _, cost = self.sess.run(
            [self._train_op, self.loss_op],
            feed_dict={
                # self.state is input of eval_net
                # self.state <- first two elements (x, y) in batch_memory
                self.state: batch_memory[:, :self.n_features],
                # self.q_target is input of self.loss_op
                self.q_target: q_eval
            })

        self.cost_his.append(cost)

        # increasing epsilon up to self.epsilon_max
        self.epsilon = min(self.epsilon + self.epsilon_increment,
                           self.epsilon_max)
        self.learn_step_counter += 1

    def plot_cost(self):
        import matplotlib.pyplot as plt
        plt.plot(np.arange(len(self.cost_his)), self.cost_his)
        plt.ylabel('Cost')
        plt.xlabel('training steps')
        plt.show()

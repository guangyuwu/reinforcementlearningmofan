"""
Sarsa is a online updating method for Reinforcement learning.
Unlike Q learning which is an offline updating method, Sarsa is updating while
in the current trajectory.
You will see the Sarsa is more coward when punishment is close because it cares
about all behaviours, while Q-learning is more brave because it only cares
about maximum behaviour.
"""

from src.rl5_deep_q_network.maze_env import Maze
# from src.rl5_deep_q_network.rl_brain import DeepQNetwork
from src.rl5_deep_q_network.rl_brain_modified import DeepQNetworkModified


def run_maze():
    step = 0
    for episode in range(300):
        # initial state
        state = env.reset()

        while True:
            # fresh env
            env.render()

            # RL choose action based on current state
            action = RL.choose_action(state)

            # RL take action and get next state and reward
            next_state, reward, done = env.step(action)

            RL.store_transition(state, action, reward, next_state)

            if (step > 200) and (step % 5 == 0):
                print(step)
                RL.learn()

            # swap state
            state = next_state

            # break while loop when end of this episode
            if done:
                print('break')
                break
            step += 1

    # end of game
    print('game over')
    env.destroy()


if __name__ == '__main__':
    # maze game
    env = Maze()
    RL = DeepQNetworkModified(env.n_actions,
                              env.n_features,
                              learning_rate=0.01,
                              reward_decay=0.9,
                              e_greedy=0.9,
                              replace_target_iter=200,
                              memory_size=2000,
                              output_graph=True
                              )
    env.after(100, run_maze)
    env.mainloop()
    RL.plot_cost()

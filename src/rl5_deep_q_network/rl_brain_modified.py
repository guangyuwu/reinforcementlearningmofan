"""
This part of code is the Deep Q Network (DQN) brain.
view the tensorboard picture about this DQN structure on: https://morvanzhou.github.io/tutorials/machine-learning/reinforcement-learning/4-3-DQN3/#modification
View more on my tutorial page: https://morvanzhou.github.io/tutorials/
Using:
Tensorflow: r1.2
"""

import numpy as np
import tensorflow as tf

from src.rl5_deep_q_network.rl_brain import DeepQNetwork

np.random.seed(1)
tf.set_random_seed(1)


# Deep Q Network off-policy
class DeepQNetworkModified(DeepQNetwork):
    def __init__(self,
                 n_actions,
                 n_features,
                 learning_rate=0.01,
                 reward_decay=0.9,
                 e_greedy=0.9,
                 replace_target_iter=300,
                 memory_size=500,
                 batch_size=32,
                 e_greedy_increment=0,
                 output_graph=False):
        # 4 actions: ['u', 'd', 'l', 'r']
        self.n_actions = n_actions
        # 2 features: (x, y)
        self.n_features = n_features
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon_max = e_greedy
        self.replace_target_iter = replace_target_iter
        self.memory_size = memory_size
        self.batch_size = batch_size
        self.epsilon_increment = e_greedy_increment
        self.epsilon = 0 if e_greedy_increment != 0 else self.epsilon_max

        # total learning step
        self.learn_step_counter = 0

        # initialize zero memory [state, action, reward, next_state]
        # shape (2000, 6)
        # 6 elements includes [state_x, state_y, action, reward, next_state_x,
        # next_state_y]
        self.memory = np.zeros((self.memory_size, n_features * 2 + 2))

        # memory counter is used to insert/update transitions
        self.memory_counter = 0

        # consist of [target_net, evaluate_net]
        self._build_net()

        t_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES,
                                     scope='target_net')
        e_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES,
                                     scope='eval_net')
        with tf.variable_scope('hard_replacement'):
            self.replace_target_op = [tf.assign(t, e) for t, e in
                                      zip(t_params, e_params)]

        self.sess = tf.Session()

        if output_graph:
            # $ tensorboard --logdir=src/rl5_deep_q_network/logs
            tf.summary.FileWriter("logs/", self.sess.graph)

        self.sess.run(tf.global_variables_initializer())
        self.cost_his = []

    def _build_net(self):
        # ------------------ all inputs ------------------------
        # number of nodes in first layer
        n_l1 = 20
        # input of current state, shape (?, 2)
        self.state = tf.placeholder(tf.float32, [None, self.n_features],
                                    name='state')
        # input of next state, shape (?, 2)
        self.next_state = tf.placeholder(tf.float32, [None, self.n_features],
                                         name='next_state')
        # input of reward, shape (?,)
        self.reward = tf.placeholder(tf.float32, [None], name='reward')
        # input of action, shape (?,)
        self.action = tf.placeholder(tf.int32, [None], name='action')

        w_initializer = tf.random_normal_initializer(0., 0.3)
        b_initializer = tf.constant_initializer(0.1)

        # ------------------ build evaluate_net ------------------
        with tf.variable_scope('eval_net'):
            # output of first layer, shape (?, 20)
            l1 = tf.layers.dense(self.state, n_l1, tf.nn.relu,
                                 kernel_initializer=w_initializer,
                                 bias_initializer=b_initializer,
                                 name='eval_l1')
            # output of second layer, shape (?, 4)
            self.q_eval = tf.layers.dense(l1, self.n_actions,
                                          kernel_initializer=w_initializer,
                                          bias_initializer=b_initializer,
                                          name='eval_l2')

        # ------------------ build target_net ------------------
        with tf.variable_scope('target_net'):
            # output of first layer, shape (?, 20)
            l1 = tf.layers.dense(self.next_state, n_l1, tf.nn.relu,
                                 kernel_initializer=w_initializer,
                                 bias_initializer=b_initializer,
                                 name='target_l1')
            # output of second layer, shape (?, 4)
            self.q_next = tf.layers.dense(l1, self.n_actions,
                                          kernel_initializer=w_initializer,
                                          bias_initializer=b_initializer,
                                          name='target_l2')

        with tf.variable_scope('q_target'):
            # shape (?,)
            q_target = self.reward + self.gamma * tf.reduce_max(
                self.q_next, axis=1, name='q_max_next_state')
            # shape (?,)
            self.q_target = tf.stop_gradient(q_target)

        with tf.variable_scope('q_eval'):
            action_indices = tf.stack(
                [tf.range(tf.shape(self.action)[0], dtype=tf.int32),
                 self.action], axis=1)
            self.q_eval_wrt_a = tf.gather_nd(params=self.q_eval,
                                             indices=action_indices)

        with tf.variable_scope('loss'):
            self.loss_op = tf.reduce_mean(
                tf.squared_difference(self.q_target, self.q_eval_wrt_a,
                                      name='error'))

        with tf.variable_scope('train'):
            self._train_op = tf.train.RMSPropOptimizer(self.lr).minimize(
                self.loss_op)

    def learn(self):
        # check to replace target parameters
        if self.learn_step_counter % self.replace_target_iter == 0:
            self.sess.run(self.replace_target_op)
            print(f'target_params_replaced '
                  f'{self.learn_step_counter // self.replace_target_iter}')

        # sample batch memory from all memory
        sample_indexes = np.random.choice(
            min(self.memory_size, self.memory_counter), size=self.batch_size)
        batch_memory = self.memory[sample_indexes, :]

        _, cost = self.sess.run(
            [self._train_op, self.loss_op],
            feed_dict={
                self.state: batch_memory[:, :self.n_features],
                self.action: batch_memory[:, self.n_features],
                self.reward: batch_memory[:, self.n_features + 1],
                self.next_state: batch_memory[:, -self.n_features:],
            })

        self.cost_his.append(cost)

        # increasing epsilon
        self.epsilon = min(self.epsilon + self.epsilon_increment,
                           self.epsilon_max)
        self.learn_step_counter += 1

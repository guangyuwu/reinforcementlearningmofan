import numpy as np
import pandas as pd


class QLearningTable:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9,
                 e_greedy=0.9):
        self.actions = actions
        self.learning_rate = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=actions, dtype=np.float64)

    def check_state_exist(self, state):
        """
        Append a new state to Q-table if it doesn't exist.
        :param state: a state
        :return: None
        """
        if state not in self.q_table.index:
            self.q_table = self.q_table.append(
                pd.Series([0] * len(self.actions),
                          index=self.q_table.columns,
                          name=state)
            )

    def choose_action(self, state):
        """
        Choose an action `a` from a state `s` using policy derived from
        Q-table (epsilon-greedy).
        :param state: current state
        :return: action `a`
        """
        self.check_state_exist(state)
        if np.random.uniform() < self.epsilon:
            state_actions = self.q_table.loc[state]
            # some actions may have the same value, randomly choose one in
            # these actions
            action = np.random.choice(
                state_actions[state_actions == np.max(state_actions)].index)
        else:
            action = np.random.choice(self.actions)
        return action

    def learn(self, state, action, reward, next_state):
        """
        Update Q-table using the reward.
        :param state: current state
        :param action: chosen action
        :param reward: reward by the action on the current state
        :param next_state: next state
        :return: None
        """
        self.check_state_exist(next_state)
        q_predict = self.q_table.loc[state, action]
        if next_state != 'terminal':
            q_target = reward + self.gamma * self.q_table.loc[next_state].max()
        else:
            q_target = reward
        self.q_table.loc[state, action] += self.learning_rate * (
                q_target - q_predict)

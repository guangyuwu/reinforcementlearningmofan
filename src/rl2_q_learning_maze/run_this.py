from src.rl2_q_learning_maze.maze_env import Maze
from src.rl2_q_learning_maze.rl_brain import QLearningTable


def update():
    for episode in range(100):
        print('-' * 20 + f' {episode} ' + '-' * 20)
        # initial state (observation)
        state = env.reset()
        while True:
            print(state)
            # fresh env
            env.render()

            # RL chooses action based on current state
            action = RL.choose_action(str(state))

            # RL takes action and get next state and reward
            next_state, reward, done = env.step(action)

            # RL learns from this transition
            RL.learn(str(state), action, reward, str(next_state))

            # swap state
            state = next_state

            # break while loop when end of this episode
            if done:
                break

    # end of game
    print('game over')
    env.destroy()


if __name__ == '__main__':
    env = Maze()
    RL = QLearningTable(actions=list(range(env.n_actions)))
    env.after(100, update)
    env.mainloop()

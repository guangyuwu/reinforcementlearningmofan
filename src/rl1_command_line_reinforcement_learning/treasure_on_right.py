import time

import numpy as np
import pandas as pd

np.random.seed(2)

N_STATES = 6  # the length of the 1 dimensional world
ACTIONS = ['left', 'right']  # available actions
EPSILON = 0.9  # greedy policy
ALPHA = 0.1  # learning rate
LAMBDA = 0.9  # discount factor
MAX_EPISODES = 13  # max episodes
FRESH_TIME = 0.1  # fresh time for one move
TERMINAL_TIME = 2  # sleeping time between episodes


def build_q_table(n_states, actions):
    """
    Initialize Q-table arbitrarily, where rows are states, and columns are
    actions.
    :param n_states: number of states
    :param actions: available actions
    :return: Q-table
    """
    q_table = pd.DataFrame(np.zeros((n_states, len(actions))), columns=actions)
    return q_table


def choose_action(state, q_table):
    """
    Choose an action `a` from a state `s` using policy derived from Q-table (
    epsilon-greedy).
    :param state: current state
    :param q_table: Q-table
    :return: action `a`
    """
    state_actions = q_table.loc[state]
    if (np.random.uniform() > EPSILON) or (state_actions.all() == 0):
        return np.random.choice(q_table.columns)
    else:
        return state_actions.idxmax()


def get_env_feedback(state, action):
    """
    Take an action `a`, observe reward `r` and next state `s'`
    :param state: current state
    :param action: chosen action
    :return: None
    """
    next_state = state + 1 if action == 'right' else max(state - 1, 0)
    reward = 1 if next_state == N_STATES - 1 else 0
    return next_state, reward


def update_env(state, episode, step_counter):
    """
    Update environment by a given state.
    :param state: new state
    :param episode: episode counter
    :param step_counter: total step counter
    :return: None
    """
    # "-----T" our environment
    evn_list = ['-'] * (N_STATES - 1) + ['T']
    if state == N_STATES - 1:
        interaction = f'Episode {episode}: total_steps = {step_counter}'
        print(f'\r{interaction}', end='')
        time.sleep(TERMINAL_TIME)
        print(f'\r{" " * N_STATES}', end='')
    else:
        evn_list[state] = 'o'
        interaction = ''.join(evn_list)
        print(f'\r{interaction}', end='')
        time.sleep(FRESH_TIME)


def update_q_table(q_table, state, action, reward, next_state):
    """
    Update Q-table using the reward.
    :param q_table: Q-table
    :param state: current state
    :param action: chosen action
    :param reward: reward by the action on the current state
    :param next_state: next state
    :return: updated Q-table
    """
    if next_state == N_STATES - 1:
        q_target = reward
    else:
        q_target = reward + LAMBDA * q_table.loc[next_state].max()
    q_predict = q_table.loc[state, action]
    q_table.loc[state, action] += ALPHA * (q_target - q_predict)
    return q_table


def rl():
    """
    Complete Q-learning algorithm.
    :return: the final Q-table
    """
    q_table = build_q_table(N_STATES, ACTIONS)
    for episode in range(1, MAX_EPISODES + 1):
        state = 0
        step_counter = 0
        update_env(state, episode, step_counter)
        while state != N_STATES - 1:
            step_counter += 1
            action = choose_action(state, q_table)
            next_state, reward = get_env_feedback(state, action)
            q_table = update_q_table(q_table, state, action, reward, next_state)
            state = next_state
            update_env(state, episode, step_counter)
    return q_table


if __name__ == "__main__":
    q_table = rl()
    print(f'\r\nQ-table:\n\n{q_table}')
